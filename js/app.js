const form = document.getElementById('artworkForm');
    const tableBody = document.querySelector('#artworkTable tbody');

    form.addEventListener('submit', function(event) {
      event.preventDefault();

      const name = document.getElementById('name').value;
      const price = document.getElementById('price').value;
      const size = document.getElementById('size').value;
      const description = document.getElementById('description').value;
      const image = document.getElementById('image').files[0];

      const reader = new FileReader();
      reader.onload = function(e) {
        const imgSrc = e.target.result;
        const newRow = createTableRow(imgSrc, name, price, size, description);
        tableBody.appendChild(newRow);
        form.reset();
      };
      reader.readAsDataURL(image);
    });

    function createTableRow(imgSrc, name, price, size, description) {
      const row = document.createElement('tr');

      const imageCell = document.createElement('td');
      const image = document.createElement('img');
      image.src = imgSrc;
      imageCell.appendChild(image);
      row.appendChild(imageCell);

      const nameCell = document.createElement('td');
      nameCell.textContent = name;
      row.appendChild(nameCell);

      const priceCell = document.createElement('td');
      priceCell.textContent = price;
      row.appendChild(priceCell);

      const sizeCell = document.createElement('td');
      sizeCell.textContent = size;
      row.appendChild(sizeCell);

      const descriptionCell = document.createElement('td');
      descriptionCell.textContent = description;
      row.appendChild(descriptionCell);

      return row;
    }